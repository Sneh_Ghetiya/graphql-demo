import { List } from "./components/List/List";
import { Add } from "./components/Add/Add";
import { Button } from "react-bootstrap";

function App() {
	return (
		<div className="App">
			<Add />

			<List />
		</div>
	);
}

export default App;
