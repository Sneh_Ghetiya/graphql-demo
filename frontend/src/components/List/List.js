import React, { useState } from "react";
import { useQuery, gql, useMutation } from "@apollo/client";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Add } from "../Add/Add";

export const GET_USERS = gql`
	{
		getAllUser {
			id
			username
			password
		}
	}
`;

const DELETE_USER = gql`
	mutation DeleteUser($id: Int!) {
		deleteUser(id: $id) {
			username
		}
	}
`;

const UPDATE_USER = gql`
	mutation UpdateUser($id: Int!, $username: String!, $password: String!) {
		updateUser(id: $id, username: $username, password: $password) {
			username
		}
	}
`;

export const List = () => {
	const { data } = useQuery(GET_USERS);
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [updateUser] = useMutation(UPDATE_USER, {
		refetchQueries: [{ query: GET_USERS }],
	});

	const [deleteUser] = useMutation(DELETE_USER, {
		refetchQueries: [{ query: GET_USERS }],
	});

	const handleClick = (id) => {
		deleteUser({ variables: { id: id } });
	};

	const handleUpdate = (id, username, password) => {
		setUsername(username);
		setPassword(password);
		
		// updateUser({
		// 	variables: { id: id, username: username, password: password },
		// });
	};
	return (
		<Container fluid="lg" className="mx-auto my-5 justify-content-md-center">
			<h2>Users List</h2>
			<Table striped bordered hover size="sm">
				<thead>
					<tr>
						<th>Username</th>
						<th>Password</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{data &&
						data.getAllUser.map((user) => (
							<tr key={user.id}>
								<td>{user.username}</td>
								<td>{user.password}</td>
								<td>
									<Row md={4} className="justify-content-md-center">
										<Col>
											<Button
												variant="warning"
												onClick={() =>
													handleUpdate(user.id, user.username, user.password)
												}
											>
												Edit
											</Button>
										</Col>
										<Col>
											<Button
												variant="danger"
												onClick={() => handleClick(user.id)}
											>
												Delete
											</Button>
										</Col>
									</Row>
								</td>
							</tr>
						))}
				</tbody>
			</Table>
		</Container>
	);
};
