import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useMutation, gql } from "@apollo/client";
import { GET_USERS } from "../List/List";
import Container from "react-bootstrap/Container";

const ADD_USER = gql`
	mutation CreateUser($username: String!, $password: String!) {
		createUser(username: $username, password: $password) {
			id
			username
			password
		}
	}
`;

export const Add = () => {
	const [username, setUsername] = React.useState("");
	const [password, setPassword] = React.useState("");

	const [addUser, { dataUpdated }] = useMutation(ADD_USER, {
		refetchQueries: [{ query: GET_USERS }],
	});

	const handleChange = (e) => {
		if (e.target.name === "username") {
			setUsername(e.target.value);
		} else if (e.target.name === "password") {
			setPassword(e.target.value);
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		addUser({ variables: { username: username, password: password } });
		if (dataUpdated) {
			console.log(dataUpdated);
		}
		setUsername("");
		setPassword("");
	};

	return (
		<Container fluid="lg" className="mx-auto my-5">
			<h2>Add User</h2>
			<Form onSubmit={handleSubmit}>
				<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label>Username</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter username"
						name="username"
						onChange={handleChange}
						value={username}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						name="password"
						onChange={handleChange}
						value={password}
					/>
				</Form.Group>
				<Button variant="primary" type="submit">
					Submit
				</Button>
			</Form>
		</Container>
	);
};
