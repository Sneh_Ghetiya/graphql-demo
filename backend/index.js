const express = require("express");
const { ApolloServer } = require("apollo-server");
const typeDefs = require("./src/schemas/schemas");
const resolvers = require("./src/resolvers/resolvers");
const cors = require("cors");

const PORT = 4000;
const ENDPOINT = "/graphql";

const server = new ApolloServer({ cors: true, typeDefs, resolvers });
server.listen().then(() => {
	console.log(`Server 🚀 on http://localhost:${PORT}${ENDPOINT}`);
});
