const { gql } = require("apollo-server-express");

const typeDefs = gql`
	type User {
		id: Int!
		username: String!
		password: String!
	}

	type Response {
		id: Int!
		updated: Boolean!
		deleted: Boolean!
	}

	type Query {
		getAllUser: [User]
		getUser(id: Int!): User
	}

	type Mutation {
		createUser(username: String!, password: String!): User
		updateUser(id: Int!, username: String!, password: String!): User
		deleteUser(id: Int!): User
	}
`;

module.exports = typeDefs;
