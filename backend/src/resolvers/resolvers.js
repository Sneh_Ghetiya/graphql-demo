const { User } = require("../models");

const resolvers = {
	Query: {
		getAllUser(_, args) {
			return User.findAll();
		},
		getUser(_, args) {
			if (args.id) {
				return User.findOne({ where: { id: args.id } });
			}
		},
	},
	Mutation: {
		async createUser(_, { username, password }) {
			const newUser = await User.create({ username, password });
			return newUser;
		},
		async updateUser(_, { id, username, password }) {
			const user = await User.findOne({ where: { id } });
			if (user) {
				await User.update({ username, password }, { where: { id } });
				return new User({ username });
			} else {
				throw new Error("User not found");
			}
		},
		async deleteUser(_, { id }) {
			const user = await User.findOne({ where: { id } });
			if (user) {
				await User.destroy({ where: { id } });
				return new User({ username: user.username });
			} else {
				throw new Error("User not found");
			}
		},
	},
};

module.exports = resolvers;
